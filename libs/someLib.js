class HelloWorld {
    sayHello(event) {
        return {
            message: 'shared library test',
            input: event,
        };
    }
}
    
module.exports = HelloWorld;