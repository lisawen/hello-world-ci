#!/usr/bin/env bash

# usage: bash -e -x ./.circleci/test_script.sh <service name>

# run the test scripts for that service
echo "Running tests for $1...";
cd services/$1;
mkdir -p libs;
mv ../../libs/* ./libs/;
npm test "$1/__tests__";
echo "Completed tests for $1";