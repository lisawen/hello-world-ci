#!/usr/bin/env bash

# usage: bash -e -x ./.circleci/test_lambda_script.sh <service name>
source deploy-env-var.sh;
cd services;

# if the service matches the service name passed in, run the test scripts for that service
for dir in ${DIR[@]};
do
    if [ "$dir" == "$1" ] && [ -d ./$dir/__lambdaTests__ ]; then
        echo "Running integration tests for $dir...";
        cd $dir;
        npm test "$dir/__lambdaTests__";
        echo "Completed integration tests for $dir";
        break;
    fi
done