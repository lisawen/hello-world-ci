#!/usr/bin/env bash

# usage: bash -e -x ./.circleci/rollback.sh <service name>
source ./sls-ts.sh;

# move to the service directory and execute a rollback
cd ./services/$1;
serverless rollback --timestamp "${SLS_TIMESTAMP}";