#!/usr/bin/env bash

# usage: bash -e -x ./.circleci/check_service.sh <service name>
source env-var.sh;
cd services;

# if the service matches the service name passed in, run the test scripts for that service
for dir in ${DIR[@]};
do
    if [ "$dir" == "$1" ] && [ -d ./$dir/__tests__ ]; then
        exit 0;
    fi
done

# exit the job early for this service
circleci step halt