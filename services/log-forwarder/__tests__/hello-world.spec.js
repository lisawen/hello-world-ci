const helloWorld = require('../functions/handler').helloWorld;


describe('helloWorld', () => {
    var event = {};
    var context = {};
    var resp = {};
    
    it('should call helloWorld function with succes', (done) => {
        var callback = (ctx, data) => {
            console.log(data);
            resp = data;
            done();
        }
        helloWorld(event, context, callback);
        expect(resp.statusCode).toBe(200);
    });
});

const HelloWorld = require('../functions/hello-world');

describe('sayHello', () => {
    var event = {};
    var hWorld = new HelloWorld();

    it('should call sayHello and return message', () => {
        expect(hWorld.sayHello(event).message).toBeDefined();
    });
});