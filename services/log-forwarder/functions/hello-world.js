const {toXml, toJson} = require('json-xml');
const json = {
    test: {
        reason: {
            desc: 'Test for stub log-forwarder service',
            test: 'test change service run'
        }
    }
};
const xml = toXml(json);
const jsonAgain = toJson(xml);

class HelloWorld {
    sayHello(event) {
        return {
            message: JSON.stringify(jsonAgain),
            input: event,
        };
    }
}
    
module.exports = HelloWorld;