const slsw = require('serverless-webpack');
const nodeExternals = require('webpack-node-externals');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const EventHooksPlugin = require('event-hooks-webpack-plugin');
const fs = require('fs-extra');

module.exports = {
    target: 'node',
    entry: slsw.lib.entries,
    externals: [nodeExternals({
        modulesDir: path.join(__dirname, '../../node_modules'),
    })],
    plugins: [
        new CopyWebpackPlugin([
            { from: '../../libs/*', to: 'service/libs', force: true }
        ], { copyUnmodified: true }),
        new EventHooksPlugin({
            'beforeRun': (compilation, done) => {
              fs.copy('../../libs', './libs', done);
            }
        })
    ]
};