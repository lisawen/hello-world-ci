const {toXml, toJson} = require('json-xml');
const json = {
    test: {
        reason: {
            desc: 'Testing using npm packages with shared packages',
            test: 'Test for stub direct-api service'
        }
    }
};
const xml = toXml(json);
const jsonAgain = toJson(xml);

class HelloWorld {
    sayHello(event) {
        return {
            message: JSON.stringify(jsonAgain),
            input: event,
        };
    }
}
    
module.exports = HelloWorld;