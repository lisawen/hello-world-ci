
const exec = require('child_process').exec;

describe('lambdaTest', () => {

    test('Test invoking lambda endpoint post-deployment', (done) => {
      // bash command to locally run the lambda
      let bash_cmd = 'sls invoke --function helloWorld -d';

      // add cwd when testing locally and not running from service folder, like so:
      // exec(bash_cmd, { cwd: './services/auth-api', encoding: 'utf-8' }
      // execute the command; should expect no errors
      exec(bash_cmd, { encoding: 'utf-8' }, (error, stdout, stderr) => {
        expect(error).toBeNull();
        expect(stderr).toBe("");

        // can add more details for expectations of stdout on a case-by-case basis for each lambda
        expect(stdout).toBeDefined();
        done();
      }
      );

    });
});