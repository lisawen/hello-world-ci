class HelloWorld {
    sayHello(event) {
        return {
            message: 'Test for stub auth-api service test changes to test merges',
            input: event,
        };
    }
}
    
module.exports = HelloWorld;