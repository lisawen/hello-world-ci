class HelloWorld {
    sayHello(event) {
        return {
            message: 'Test for stub provider-api service',
            input: event,
        };
    }
}
    
module.exports = HelloWorld;