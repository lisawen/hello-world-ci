class HelloWorld {
    sayHello(event) {
        return {
            message: 'Test for stub patient-api service',
            input: event,
        };
    }
}
    
module.exports = HelloWorld;