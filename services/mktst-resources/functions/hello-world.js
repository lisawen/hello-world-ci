class HelloWorld {
    sayHello(event) {
        return {
            message: 'Test for stub mktst-resources service',
            input: event,
        };
    }
}
    
module.exports = HelloWorld;