class HelloWorld {
    sayHello(event) {
        return {
            message: 'Test for stub subscription-api service',
            input: event,
        };
    }
}
    
module.exports = HelloWorld;